#include <iostream>
#include <fstream>
#include <string>

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

#define FILENAME "dpt2018.csv"

#define SQL_SERVER "remotemysql.com"
#define SQL_USER "HAJaXVQlYw"
#define SQL_PWD "J3TcO2yMwF"
#define SQL_BASE "HAJaXVQlYw"




using namespace std;

sql::Statement *sql_connect()
{
	sql::Driver *driver;
	sql::Connection *con;
	sql::Statement *stmt;

	/* Create a connection */
	driver = get_driver_instance();
	con = driver->connect(SQL_SERVER, SQL_USER, SQL_PWD);
	/* Connect to the MySQL test database */
	con->setSchema(SQL_BASE);
	stmt = con->createStatement();
	//delete con; // -> close the connection
	return stmt;
}

bool free_table(string table_name)
{
	try
	{
		sql::Statement *stmt = sql_connect();
		bool res = stmt->execute("DELETE FROM `"+table_name+"`");
		delete stmt;
		return res;
	} catch (sql::SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
	return false;
}

bool insert_name(string line)
{
	try
	{
		sql::Statement *stmt = sql_connect();
		string name = "toto";
		string year = "2345";
		cout<<line<<endl;
		bool res= true;
		//bool res = stmt->execute("INSERT INTO name VALUES ('"+name+"', '"+year+"')");
		delete stmt;
		return res;
	} catch (sql::SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
	return false;
}

int main()
{
	std::cout<<"START parsing file "<<FILENAME<<std::endl;

	std::ifstream csv_file(FILENAME);
	if(csv_file.is_open()){
		std::cout<<"Open file "<<FILENAME<<std::endl;

		free_table("name");
		string line;
		while(getline(csv_file,line)){
			insert_name(line);
		}

	}
	else{
		std::cerr<<"Cannot open file "<<FILENAME<<std::endl;
	}

	try {

	} catch (sql::SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}

	cout << endl;

	csv_file.close();
}
